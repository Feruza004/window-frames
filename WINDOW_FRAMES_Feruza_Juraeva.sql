/*
 	Feruza Juraeva
    Sales Report Query
    Description:
    This query generates a sales report for the 49th, 50th, and 51st weeks of 1999. 
    It includes a cumulative sum of sales for each week and a centered 3-day moving average 
    for sales, with special handling for Mondays and Fridays.
*/

-- Outer query selects the final columns for the report
SELECT
	week_number AS calendar_week_number,-- Week number of the year
	time_id, 							-- Date of the transaction
	day_of_week AS day_name, 			-- Day of the week
	total_sales AS sales,				-- Total sales for the day
	cumulative_sum AS cum_sum, 			-- Cumulative sum of sales for the week, 
	ROUND( 								-- Conditional calculation for the centered 3-day moving average,
        CASE							-- -rounded to 2 decimal places
		WHEN EXTRACT(isodow
	FROM
		time_id) = 1 THEN 				-- For Monday, include the weekend and Tuesday
		AVG(total_sales) OVER (
                    PARTITION BY week_number
	ORDER BY
		time_id
                    ROWS BETWEEN 2 PRECEDING AND 1 FOLLOWING
                )
		WHEN EXTRACT(isodow
	FROM
		time_id) = 5 THEN				-- For Friday, include Thursday and the weekend
		AVG(total_sales) OVER (
                    PARTITION BY week_number
	ORDER BY
		time_id
                    ROWS BETWEEN 1 PRECEDING AND 2 FOLLOWING
                )
		ELSE 							-- For other days, include the previous, current, and next day
		AVG(total_sales) OVER (
                    PARTITION BY week_number
	ORDER BY
		time_id
                    ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING
                )
	END,
	2
    ) AS CENTERED_3_DAY_AVG
FROM
	( 									-- Subquery to calculate total sales and cumulative sum per day
	SELECT
		EXTRACT(week
	FROM
		time_id) AS week_number,		-- Week number extracted from the date
		time_id,						-- Date of the transaction
		to_char(time_id,
		'Day') AS day_of_week,			-- Day of the week in textual format
		SUM(amount_sold) AS total_sales,-- Total sales for each day
		SUM(SUM(amount_sold)) OVER (	-- Window function to calculate the cumulative sum of sales for the week
            PARTITION BY EXTRACT(week
	FROM
		time_id)
	ORDER BY
		time_id
            RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
        ) AS cumulative_sum
	FROM
		sh.sales
	WHERE
		EXTRACT(YEAR
	FROM
		time_id) = 1999
		AND								-- Filtering for the year 1999
        EXTRACT(week
	FROM
		time_id) BETWEEN 49 AND 51		-- Including only weeks 49, 50, and 51
	GROUP BY
		EXTRACT(week
	FROM
		time_id),
		time_id
) AS weekly_sales
ORDER BY								-- Ordering by week number and date
	week_number,
	time_id;
-- End of the sales report query
